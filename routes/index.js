var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var bcrypt = require('bcrypt');
var conn = require('../database/conn');
const emailValidator = require('email-validator');
const valid_test='testing';

/* GET home page. */
router.get('/',function(req, res, next){
  if(req.session.flag == 1){
      req.session.destroy();
      res.render('index', { title: 'Create your account', message : 'Invalid Name', flag : 1});
  }
  
  else if(req.session.flag == 2){
      req.session.destroy();
      res.render('index', { title: 'Create your account', message : 'Invalid Password', flag : 1});
  }
  else if(req.session.flag == 3){
      req.session.destroy();
      res.render('index', { title: 'Create your account', message : 'email Already Exists', flag : 1});
  }
  else if(req.session.flag == 4){
      req.session.destroy();
      res.render('index', { title: 'Create your account', message : 'Registration Done', flag : 0})
  }
  else if(req.session.flag == 5){
    req.session.destroy();
    res.render('index', { title: 'Create your account', message : 'Confirm password Does Not Match', flag : 1});
}
else if(req.session.flag == 6){
    req.session.destroy();
    res.render('index', { title: 'Create your account', message : 'Invalid Email', flag : 1});
}
else if(req.session.flag == 7){
    req.session.destroy();
    res.render('index', { title: 'Create your account', message : 'Incorrect Email or password', flag : 1});
}
  else{
      res.render('index', { title: 'Create your account'});
  }
  
});

//handle POST request for user registration
router.post('/auth_reg', function(req, res, next){

  var fullname = req.body.fullname;
  if(!fullname){
    req.session.flag = 1;
    return res.redirect('/');            
 }

  var password = req.body.password;
  if(!password){
    req.session.flag = 2;
    return res.redirect('/');
 }

    var email = req.body.email;

  if(emailValidator.validate(email)){
       var cpassword = req.body.cpassword;

      if(cpassword == password){
      var sql = 'select * from user where email = ?;';
      conn.query(sql,[email], function(err, result, fields){
          if(err) throw err;

          if(result.length > 0){
              req.session.flag = 3;
              res.redirect('/');
          }
          else{

              var hashpassword = bcrypt.hashSync(password, 10);
              var sql = 'insert into user(fullname, email, password) values(?,?,?);';
              conn.query(sql,[fullname,email,hashpassword], function(err, result, fields){
                  if(err) throw err;
                  req.session.flag = 4;
                  res.redirect('/');
              });
          }
      });

  }else{
      req.session.flag = 5;
      res.redirect('/');
  }
 } else {
  req.session.flag = 6;
  res.redirect('/');
 }
});



//Handle POST request for user login
router.post('/auth_login', function(req, res, next){
    
  var email = req.body.email;
  var password = req.body.password;

  var sql = "select * from user where email =?;";

  conn.query(sql, [email], function(err, result, fields){
      if(err) throw err;
      
      if(result.length && bcrypt.compareSync(password, result[0].password)){
          req.session.user_id = result[0].id;
          res.redirect('/home');
      }else{
          req.session.flag = 7;
          res.redirect('/home');
      }
  });
});

//Routr for Home page
router.get('/home', function(req, res, next){
  var user_id = req.session.user_id;
  if(!user_id) {
    res.redirect('/');
  }

  // res.render('home', { title: 'Add your book'});
  var sql = "select book.user_id,book.id, book.book, book.bookauthor FROM book where user_id =?";
  conn.query(sql, [user_id], (err, rows, fields) =>{
    if(!err)
         {

            res.render('home', { title: 'Add your book', record_listing:rows});

            
            //res.send(rows);
         }
         else
         {
            console.log(err);
            
         }
      });

});

router.get('/book', function(req, res, next){
  var title = 'Add your book';
  if(req.session.flag == 1){
    req.session.destroy();
    res.render('book', { title: title, message : 'Invalid Book name', flag : 1});
  }
  else if(req.session.flag == 2){
    req.session.destroy();
    res.render('update', { title: title, message : 'Invalid bookauthor name', flag : 1})
}
else if(req.session.flag == 3){
  req.session.destroy();
  res.render('update', { title:  title, message : 'book Already Exists', flag : 1})
}
    res.render('book', { title: title});
});

router.post('/book', function(req, res, next){
  var book = req.body.book;
  if(!book){
    req.session.flag = 1;
    return res.redirect('/book');
 }
  var bookauthor = req.body.bookauthor;
  if(!bookauthor){
    req.session.flag = 2;
    return res.redirect('/book');
 }
  var user_id = req.session.user_id;
  var id = req.query.id;
  
  var sql = 'select id from book where book = ?;';
  conn.query(sql, [book], function(err, result, fields){
    if(err) throw err;
    
    if(result.length > 0) {
      req.session.flag = 3;
      res.redirect('/book');
    }
    else{
      if(!id){
        var sql = 'INSERT INTO book (book,user_id,bookauthor) VALUES(?,?,?);';
        conn.query(sql,[book, user_id, bookauthor],(err, rows, result, fields) =>{
          if(!err)
          {
            res.redirect('/home');
          }
          else
          {
              console.log(err);
              res.send(err);
          }
        });
      }else{ 
        var sql = "UPDATE book SET book=(?),bookauthor=(?) WHERE id=(?)";
        conn.query(sql,[book, bookauthor, id],(err, rows, result, fields) =>{
          if(!err)
          {
            res.redirect('/home');
          }
          else
          {
              console.log(err);
              res.send(err);
          }
        });
      }
    }
 });
});

// router.post('/home', function(req, res, next){
//   var book = req.body.book;
// 
//   var bookauthor = req.body.bookauthor;
//   
//   // var user_id = req.session.user_id;
//   var id = req.body.id;
//   
  
  

router.get('/update', function(req, res, next){
  var id = req.query.id;

  if(req.session.flag == 1){
    req.session.destroy();
    res.render('update', { title: 'Add your book', message : 'Invalid Book name', flag : 1});
  }
  else if(req.session.flag == 2){
    req.session.destroy();
    res.render('update', { title: 'Add your book', message : 'Invalid bookauthor name', flag : 1})
}
  else if(req.session.flag == 3){
    req.session.destroy();
    res.render('update', { title: 'Add your book', message : 'book Already Exists', flag : 1})
}
res.render('update', { title: 'Add your book',record_listing:id});
    
});

router.post('/update', function(req, res, next){
  var book = req.body.book;
  if(!book){
    req.session.flag = 1;
    return res.redirect('/update');
 }
  var bookauthor = req.body.bookauthor;
  if(!bookauthor){
    req.session.flag = 2;
    return res.redirect('/update');
 }
  var id = req.query.id;
  var sql = 'select id from book where book = ?;';
  conn.query(sql, [book], function(err, result, fields){
    if(err) throw err;
    
    if(result.length > 0) {
      req.session.flag = 3;
      res.redirect('/update');
    }
else{
  if(id){
  var sql = "UPDATE book SET book=(?),bookauthor=(?) WHERE id=(?)";
  conn.query(sql,[book, bookauthor, id],(err, rows, result, fields) =>{
    if(!err)
         {
          res.redirect('/home');
         }
         else
         {
            console.log(err);
            res.send(err);
         }
        //  res.redirect('/update');
        
    });
  }else{
    res.redirect('/home');
  }
}
  });
});

router.get('/delete', function(req, res, next){
  var id = req.query.id;
  if(id){
    res.render('delete', { title: 'Add your book',record_listing:id});
  } else {
    res.redirect('/home');
  } 
});

router.post('/delete', function(req, res, next){
  // var book = req.body.book;
  // var bookauthor = req.body.bookauthor;
  // var user_id = req.session.user_id;
  var id = req.query.id;
  if(id){
  var sql = "DELETE FROM book  WHERE id=(?)";
  conn.query(sql, [id], (error,rows, fields)=>{
    if(!error)
    {
      res.redirect('/home');
    }
    else
    {
       console.log(error);
       response.send(error);
    }
});
}else{
  res.redirect('/home');
}
});

router.get('/logout', function(req, res, next){
if(req.session.user_id){
  req.session.destroy();
} 
  res.redirect('/');

});



module.exports = router;
